##### 
# SET UP ENVIRONMENT
. ../setup.sh

export GCC_VER=4.8.2

# MAGMA for QUDA
#export MAGMA_PATH=/dist/magma_gcc-5.3.0-cuda-8.0-regular-blas
export MAGMA_PATH=/dist/magma_gcc-${GCC_VER}-regular-blas
export PKG_CONFIG_PATH=${MAGMA_PATH}/lib/pkgconfig:$PKG_CONFIG_PATH
export CUDA_INSTALL_PATH=/usr/local/cuda-7.0
#export CUDA_INSTALL_PATH=/usr/local/cuda-8.0
MPIHOME=/usr/mpi/gcc/mvapich2-1.8
#export MPIHOME=/usr/mpi/gcc/mvapich2-2.0rc1
OMP="yes"

export MAGMA_INC=`pkg-config magma --cflags-only-I`
export MAGMA_LIB=`pkg-config magma --libs`
echo MAGMA_INC is ${MAGMA_INC}
echo MAGMA_LIB is ${MAGMA_LIB}

# SM=sm_35     # Kepler GK110
# SM=sm_20   # Fermi
# SM=sm_13   # Pre-Fermi, with DP support eg. GTX285
SM=sm_35     # Kepler Gaming

export PATH=${CUDA_INSTALL_PATH}/bin:${MPIHOME}/bin:$PATH
export LD_LIBRARY_PATH=${CUDA_INSTALL_PATH}/lib64:${MPIHOME}/lib64:${MPIHOME}/lib:${CUDA_INSTALL_PATH}/lib:/usr/lib64:/usr/lib:$LD_LIBRARY_PATH
export PATH=/dist/gcc-${GCC_VER}/bin:$PATH
export LD_LIBRARY_PATH=/dist/gcc-${GCC_VER}/lib64:/dist/gcc-${GCC_VER}/lib:/usr/lib64:/usr/lib:$LD_LIBRARY_PATH

# The directory containing the build scripts, this script and the src/ tree
TOPDIR=`pwd`

# Install directory
INSTALLDIR=${TOPDIR}/install/${SM}

# Source directory
SRCDIR=${TOPDIR}/../src

# Build directory
BUILDDIR=${TOPDIR}/build


### ENV VARS for CUDA/MPI
# These are used by the configure script to make make.inc
PK_CUDA_HOME=${CUDA_INSTALL_PATH}
PK_MPI_HOME=${MPIHOME}
PK_GPU_ARCH=${SM}

### OpenMP
# Open MP enabled
if [ "x${OMP}x" == "xyesx" ]; 
then 
 OMPFLAGS="-fopenmp -D_REENTRANT "
 OMPENABLE="--enable-openmp"
 INSTALLDIR=${INSTALLDIR}_omp
else
 OMPFLAGS=""
 OMPENABLE=""
fi

if [ ! -d ${INSTALLDIR} ];
then
  mkdir -p ${INSTALLDIR}
fi
### COMPILER FLAGS
PK_CXXFLAGS=${OMPFLAGS}" -g -O3 -finline-limit=50000 -std=c++11 -march=corei7-avx"

PK_CFLAGS=${OMPFLAGS}" -g -O3 -std=gnu99"

### Make
MAKE="make -j 10"

### MPI
PK_CC=mpicc
PK_CXX=mpicxx
#PK_NVCCFLAGS="NVCCFLAGS=\"--keep --keep-dir=/scratch/bjoo/tmp\""
