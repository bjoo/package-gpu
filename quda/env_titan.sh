##### 
# SET UP ENVIRONMENT

#export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:/ccs/home/bjoo/magma-1.7.0-install-netlib-lapack/lib/pkgconfig
#export MAGMA_INC=`pkg-config magma --cflags`
#export MAGMA_LIB=`pkg-config magma --libs`

module unload PrgEnv-pgi
module load PrgEnv-gnu
module load cudatoolkit
module load cmake3/3.9.0

SM=sm_35     # Kepler Gaming
OMP="yes"

# The directory containing the build scripts, this script and the src/ tree
TOPDIR=`pwd`

# Install directory
INSTALLDIR=${TOPDIR}/install/${SM}

# Source directory
SRCDIR=${TOPDIR}/../src

# Build directory
BUILDDIR=${TOPDIR}/build


### ENV VARS for CUDA/MPI
# These are used by the configure script to make make.inc
PK_CUDA_HOME=${CUDATOOLKIT_HOME}
PK_MPI_HOME=${MPIHOME}
PK_GPU_ARCH=${SM}

### OpenMP
# Open MP enabled
if [ "x${OMP}x" == "xyesx" ]; 
then 
 OMPFLAGS="-fopenmp -D_REENTRANT "
 OMPENABLE="--enable-openmp"
 INSTALLDIR=${INSTALLDIR}_omp
else
 OMPFLAGS=""
 OMPENABLE=""
fi

if [ ! -d ${INSTALLDIR} ];
then
  mkdir -p ${INSTALLDIR}
fi
### COMPILER FLAGS
PK_CXXFLAGS=${OMPFLAGS}" -g -O3 -std=c++11 -march=bdver1 -mprefer-avx128 "

PK_CFLAGS=${OMPFLAGS}" -g -O3 -std=gnu99 -march=bdver1 -mprefer-avx128 "

### Make
MAKE="make -j 6"

### MPI
PK_CC=cc
PK_CXX=CC
#PK_NVCCFLAGS="NVCCFLAGS=\"--keep --keep-dir=/scratch/bjoo/tmp\""
