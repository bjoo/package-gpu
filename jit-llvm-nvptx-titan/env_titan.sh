##### 
# SET UP ENVIRONMENT
module unload PrgEnv-pgi
module load PrgEnv-gnu
module load cudatoolkit
#module load cudatoolkit/7.5.18-1.0502.10743.2.1
module load python
module load cmake3/3.9.0
module load git
module load atp
module list
# having a problem with mpi module
SM=sm_35     # Kepler Gaming
OMP="yes"


# The directory containing the build scripts, this script and the src/ tree
TOPDIR=`pwd`

# Install directory
INSTALLDIR=${TOPDIR}/install/${SM}


if [ "x${OMP}x" == "xyesx" ];
then
 INSTALLDIR=${INSTALLDIR}_omp
fi

# LLVM Install Directory
LLVM_INSTALL_DIR_FE=${INSTALLDIR}/llvm-6.0.0-fe
LLVM_INSTALL_DIR=${INSTALLDIR}/llvm-6.0.0

# Source directory
SRCDIR=${TOPDIR}/../src

# Build directory
BUILDDIR=${TOPDIR}/build


### ENV VARS for CUDA/MPI
export PK_CUDA_HOME=$CUDATOOLKIT_HOME
# These are used by the configure script to make make.inc
export LD_LIBRARY_PATH=${PK_CUDA_HOME}/nvvm/lib:${LD_LIBRARY_PATH}
PK_GPU_ARCH=${SM}

### OpenMP
# Open MP enabled
if [ "x${OMP}x" == "xyesx" ]; 
then 
 OMPFLAGS="-fopenmp -D_REENTRANT "
 OMPENABLE="--enable-openmp"
else
 OMPFLAGS=""
 OMPENABLE=""
fi

if [ ! -d ${INSTALLDIR} ];
then
  mkdir -p ${INSTALLDIR}
fi
### COMPILER FLAGS
PK_CXXFLAGS=${OMPFLAGS}" -g -O3 -std=c++11 -dynamic "
PK_CFLAGS=${OMPFLAGS}" -g -O3 -std=gnu99 -dynamic "
PK_LDFLAGS="${OMPFLAGS} -L${PK_CUDA_HOME}/lib64 -L${PK_CUDA_HOME}/nvvm/lib"
PK_LIBS="-ldl -lpthread"
### Make
MAKE="make -j 6"

### MPI
PK_CC=cc
PK_CXX=CC
QDPJIT_HOST_ARCH="X86;NVPTX"
PK_LLVM_CXX=g++
PK_LLVM_CC=gcc
PK_LLVM_CFLAGS="-O3 -std=c99"
PK_LLVM_CXXFLAGS="-O3 -std=c++11"
JITARCHS="${QDPJIT_HOST_ARCH},nvptx"
