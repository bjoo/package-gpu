#!/bin/bash

for dir in jit-llvm-nvptx jit-llvm-nvptx-titan  quda;
do
  echo "Entering Directory: ${dir}"
  pushd $dir
  if [ -d ./build ];
  then 
    echo "Removing Build Directory: ./build"
    rm -rf ./build
  fi
  if [ -d ./install ]; 
  then 
     echo "Removing Install Directory: ./install"
     rm -rf ./install
  fi
   
  echo "Remaking Empty Directories for build and install"
  mkdir ./build
  mkdir ./install
  
  echo "Leaving Directory: ${dir}"
  popd 
done
